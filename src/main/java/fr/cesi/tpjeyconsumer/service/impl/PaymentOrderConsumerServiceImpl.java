package fr.cesi.tpjeyconsumer.service.impl;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cesi.tpjeyconsumer.dto.PaymentOrderDTO;
import fr.cesi.tpjeyconsumer.service.PaymentOrderConsumerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class PaymentOrderConsumerServiceImpl implements PaymentOrderConsumerService {

  /**
   * Listen message broker queue and log message when a new message is inserted.
   *
   * @param message
   */
  @JmsListener(
      destination = "${message-broker.topic-name}",
      containerFactory = "paymentOrderContainerFactory")
  public void receive(String message) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      PaymentOrderDTO paymentOrderDTO = objectMapper.readValue(message, PaymentOrderDTO.class);
      log.info(
          String.format(
              "Recieved payment order from : %s, amount : %s",
              paymentOrderDTO.getCustomerName(), paymentOrderDTO.getAmount()));
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }
}
