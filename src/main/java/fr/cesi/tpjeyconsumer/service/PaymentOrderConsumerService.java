package fr.cesi.tpjeyconsumer.service;

public interface PaymentOrderConsumerService {
    void receive(String message);
}
